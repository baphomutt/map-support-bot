const addAWA = require('./addAWA')
const addMinor = require('./addMinor')
const deleteMinor = require('./deleteMinor')

module.exports = {
  commands: { 'Administrator Commands': { addAWA } },
}
