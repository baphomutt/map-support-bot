const getUserByUsername = require('../../utils/getUserByUsername')
const getChannelsByChannelName = require('../../utils/getChannelsByChannelName')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const inviteUserToChannels = require('../../utils/inviteUserToChannels')

const defaultChannels = [
  '--announcements--',
  'creativity-corner',
  'food',
  'fun-and-games',
  'general',
  'humor-and-jokes',
  'in-the-news',
  'introductions',
  'life-experiences',
  'literature',
  'map-talk',
  'media',
  'minor-identifying-people-map',
  'miscellaneous',
  'movies-and-tv',
  'music',
  'non-map-ally-chat',
  'requests-for-support-r1-map',
  'requests-for-support-r2-map',
  'serious',
  'spam',
  'stem',
  'suggestions',
  'suggestions-discussions',
  'tech',
  'venting-map',
  'video-games',
]

async function addMAP({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does not already have the MAP role
  if (!userHasRole(targetUser, 'MAP')) {
    // Add the MAP role to targetUser
    await bot.sendToRoom(
      'Giving ' + targetUser.name + ' the MAP role...',
      roomID,
    )
    await addUserToRole(targetUser, 'MAP')

    // Invite the targetUser to default channels
    let channels = await getChannelsByChannelName(defaultChannels)
    await bot.sendToRoom(
      'Inviting ' + targetUser.name + ' to the default channels...',
      roomID,
    )
    let invites = await inviteUserToChannels(targetUser, channels)
    invites.forEach((invite) => {
      console.log('THIS IS A INVITE OR AN ERROR FOR DEBUGGING:', invite)
    })
    await bot.sendToRoom(
      'Finished inviting ' + targetUser.name + ' to the default channels! :D',
      roomID,
    )
  } else {
    // User already has the MAP role, no need to do anything else
    const response = targetUser.name + ' already has the MAP role'
    const sentMsg = await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description:
    'Give a member the MAP role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addMAP <username>`,
  requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
  call: addMAP,
}
