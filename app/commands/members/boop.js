const getUserByUsername = require('../../utils/getUserByUsername')

async function boop({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the arguments
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Send the message
  message = await bot.sendToRoom(`_Boops ${targetUser.name}!_`, roomID)
}

module.exports = {
  description: 'Boop someone!',
  help: `${process.env.ROCKETCHAT_PREFIX} boop <user>`,
  call: boop,
}
