const getUserByUsername = require('../../utils/getUserByUsername')
const reactToMessage = require('../../utils/reactToMessage')

// A two-option poll feature,
// one that does simple yes/shrug/no
// and one that does 1-9 numbered options.

const emojiNumbers = [
  'one',
  'two',
  'three',
  'four',
  'five',
  'six',
  'seven',
  'eight',
  'nine',
]

async function poll({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the arguments
  const question = context.argumentList[0]
  const answers = context.argumentList.slice(1)

  if (answers.length === 0) {
    // handle yes/shrug/no poll
    let message = await bot.sendToRoom(question, roomID)

    // Add reactions
    await reactToMessage('thumbsup', message)
    await reactToMessage('shrug', message)
    await reactToMessage('thumbsdown', message)
  } else if (answers.length > 0) {
    // handle numbered poll
    let message = `${question}\n`

    // Add the possible answers to the message
    for (let answer in answers) {
      message = message + `:${emojiNumbers[answer]}: ${answers[answer]}\n`
    }

    // Send the message
    message = await bot.sendToRoom(message, roomID)

    // Add reactions
    for (let answer in answers) {
      await reactToMessage(emojiNumbers[answer], message)
    }
  }
}

module.exports = {
  description:
    'A two-option poll feature, one that does simple yes/shrug/no and one that does 1-9 numbered options.',
  help: `${process.env.ROCKETCHAT_PREFIX} poll <question> <optional up to 9 answers>`,
  call: poll,
}
