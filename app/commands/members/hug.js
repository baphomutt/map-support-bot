const getUserByUsername = require('../../utils/getUserByUsername')

async function hug({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the arguments
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Send the message
  message = await bot.sendToRoom(`_Hugs ${targetUser.name}! :hug:_`, roomID)
}

module.exports = {
  description: 'Hug someone!',
  help: `${process.env.ROCKETCHAT_PREFIX} hug <user>`,
  call: hug,
}
