const getUserByUsername = require('../../utils/getUserByUsername')
const deleteMessage = require('../../utils/deleteMessage')

async function feedback({ bot, message, context }) {
  const roomID = message.rid
  const feedback_channelID = '6YBHQF3HWWuNRWFLH'

  // Get the feedback message by removing "PREFIX COMMAND " from the message
  const feedback_message = message.msg.substring(
    `${context.prefix} ${context.commandName} `.length,
  )

  // Check if the room type is a direct message room
  if (message.roomType === 'd') {
    // Send the feedback to the feedback channel
    let message_to_send = '***New feedback received!*** \n * '
    message_to_send = `${message_to_send} ${feedback_message} \n`
    await bot.sendToRoom(message_to_send, feedback_channelID)

    // Tell the user that the feedback has been received
    await bot.sendToRoom('Thank you for your valuable feedback! 📬', roomID)
  } else {
    // If it isn't a direct message room

    // Delete the user's message
    await deleteMessage(roomID, message._id)

    // Send a direct message to the user what feedback they attempted to send
    let message_to_send =
      '***You tried to send the following feedback:*** \n * '
    message_to_send = `${message_to_send} ${feedback_message} \n`
    await bot.sendDirectToUser(message_to_send, message.u.username)

    // Tell the user how to send the feedback correctly,
    // first in DM, then in the public channel
    await bot.sendDirectToUser(
      'But, in order for feedback to be anonymous, please call the feedback command by sending me a DM here! 🕵',
      message.u.username,
    )
    await bot.sendToRoom(
      'In order for feedback to be anonymous, please call the feedback command by sending me a DM! 🕵',
      roomID,
    )
  }
}

module.exports = {
  description:
    "Send anonymous feedback to staff, this can be about absolutely anything you'd like to tell, positive or negative. You don't necessarily need to send feedback, but can send any message you'd like. The bot will require you to call this command by sending a DM to the bot.",
  help: `${process.env.ROCKETCHAT_PREFIX} feedback <message>`,
  cooldown: 60,
  call: feedback,
}
