const { api } = require('@rocket.chat/sdk')

const getChannelByChannelName = async (channelName) => {
  let result = await api.get('channels.info', { roomName: channelName })
  if (result.success) {
    return result.channel
  } else {
    console.error(result)
  }
}

module.exports = getChannelByChannelName
