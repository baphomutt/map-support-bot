# MAP Support Bot

This is the code for MSC Chat's MSC Bot!

# Roadmap

**_This section needs expanding on_**

# How to contribute

Anyone is allowed to contribute to the bot!

If there's a feature you'd like to be added, create an Issue. We can then
discuss your idea 😃 Anyone can also program a feature (maybe one that solves an
issue) by forking this repository and then making a merge request to the `dev`
branch. 😀

## How to setup your developer environment

**_This section needs expanding on_**

- Setup a personal Rocket Chat instance

## How to contribute anonymously

**_This section needs expanding on_**

Due to the nature of this topic, you might want to make sure that your
contributions aren't tied to your real life identity.

- Make sure that your GitLab account isn't tied to your IRL identity
- When commiting staged changes, make sure that your git config `user.email` and
  `user.name` are set to anonymous values. You can do this on a per repository
  basing with these commands: `git config --local user.name "My Name"` and
  `git config --local user.email "My email@example.com"`

## Coding Style

**_This section needs expanding on_**

## Overview of repository structure

**_This section needs expanding on_**
